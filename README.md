# NCANDA Functions

## Instructions for Use
```
# load in functions 
source("https://gitlab.com/OHSU-CMHI/ncanda-functions/-/raw/main/NCANDA_functions.R") 
```

Read below for more information about the functions included.

## Available Functions (1.0)
- [merge_ncanda_data](https://gitlab.com/ohsu-cmhi/ncanda-functions/-/blob/main/README.md#merge_ncanda_data-merge-ncanda-data-from-different-release-years)

### merge_ncanda_data: merge NCANDA data from different release years
#### Usage
```
merge_ncanda_data(path, filenames)
```
#### Arguments
**path:** path to folder where the NCANDA releases are kept (ex. '/private/tmp/shaosVolumes/OHSU/OHSU Shared/Restricted/SHARED/PSYCH/NagelLab/N-CANDA/Data/Data releases/')

**filenames:** names of .csv files you want merged across release years (ex. c('demographics.csv', 'lh.aparc.csv'))

**years:** (optional) years you want to merge as numeric vector (by default all years) (ex. c(0:1, 7:9))

**out_path:** (optional) output path for merged dataframes (ex. 'data/')
#### Value
the files will be merged across release years and saved (to the working directory or specified output directory)
#### Examples
```
# Example 1
path = '/private/tmp/shaosVolumes/OHSU/OHSU Shared/Restricted/SHARED/PSYCH/NagelLab/N-CANDA/Data/Data releases/'
filenames <- c("demographics.csv", "lh.aparc.csv")

merge_ncanda_data(path = path, filenames = filenames)

# Example 2
merge_ncanda_data(path = path, filenames = filenames, years = c(1,3,8:9), out_path = 'data/')
```

